# LowPolySHMUP
---
This is the source code and files for the game I created for the UDH Week 1 Game Jam.
Head over to [Itch.io](https://exanite.itch.io/udhweek1-ScorchedPolys) for the actual compiled game!