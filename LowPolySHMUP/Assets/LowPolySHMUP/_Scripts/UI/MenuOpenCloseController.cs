﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite;

public class MenuOpenCloseController : MonoBehaviour 
{
	public bool showMenu = false;
	private bool m_menuIsShown = false;
	private bool m_menuIsChanging = false;

	private void Start() 
	{
		if(showMenu)
		{
			gameObject.transform.localScale = Vector3.one;
			m_menuIsShown = true;
		}
		else
		{
			gameObject.transform.localScale = Vector3.zero;
			m_menuIsShown = false;
		}
	}

	private void Update()
	{
		if(!m_menuIsChanging)
		{
			if(showMenu && !m_menuIsShown)
			{
				print("Showing menu");
				StartCoroutine(MenuVisibility(true));
			}
			else if(!showMenu && m_menuIsShown)
			{
				print("Hiding menu");
				StartCoroutine(MenuVisibility(false));
			}
		}
	}

	IEnumerator MenuVisibility(bool visible, float duration = 1f, List<Vector3> midTargets = null)
	{
		float timePassed = 0f;
		m_menuIsChanging = true;

		#region SetTargets
		List<Vector3> targets = new List<Vector3>();
		targets.Add(new Vector3(0f, 0f, 1f));
		if(midTargets == null)
		{
			targets.Add(new Vector3(0.05f, 1f, 1f));
		}
		else
		{
			targets.AddRange(midTargets);
		}
		targets.Add(new Vector3(1f, 1f, 1f));

		if(!visible) 
		{
			targets.Reverse();
		}
		#endregion
		int currentTarget = 1;
		float durationScaled = duration / (targets.Count - 1);
		gameObject.transform.localScale = targets[0];

		while(gameObject.transform.localScale != targets[targets.Count - 1])
		{
			timePassed += Time.unscaledDeltaTime;
			
			if(gameObject.transform.localScale != targets[currentTarget])
			{
				gameObject.transform.localScale = VectorThree.SmoothStep(gameObject.transform.localScale, targets[currentTarget], timePassed/durationScaled);
			}
			else
			{
				currentTarget++;
				timePassed = 0f;
			}
			yield return null;
		}

		m_menuIsShown = visible;
		m_menuIsChanging = false;
	}
}
