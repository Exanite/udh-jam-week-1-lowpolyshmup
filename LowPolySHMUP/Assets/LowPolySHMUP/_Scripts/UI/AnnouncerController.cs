﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Exanite;

public class AnnouncerController : MonoBehaviour 
{
	private Text m_text;

	private void Start() 
	{
		transform.localScale = new Vector3(0f, 0f, 1f);
		m_text = GetComponent<Text>();
		m_text.text = "";
		m_text.enabled = true;
	}

	private int calls = 0;

	public IEnumerator ShowAnnouncement(string announcement, float animationDuration, float waitDuration, bool canBeInterrupted = true)
	{
		calls++;
		yield return null;
		if(calls > 1)
		{
			calls--;
			yield break;
		}

		float timePassed = 0f;
		transform.localScale = new Vector3(0f, 1f, 1f);
		m_text.text = announcement;
		
		while(transform.localScale != new Vector3(1f, 1f, 1f) || timePassed <= (animationDuration + waitDuration))
		{
			if(canBeInterrupted)
			{
				if(calls > 1)
				{
					calls--;
					yield break;
				}
			}
			timePassed += Time.deltaTime;
			if(transform.localScale != new Vector3(1f, 1f, 1f))
			{
				transform.localScale = VectorThree.SmoothStep(transform.localScale, new Vector3(1f, 1f, 1f), timePassed/animationDuration);
			}
			yield return null;
		}
		transform.localScale = new Vector3(0f, 0f, 1f);
		calls--;
	}
}
