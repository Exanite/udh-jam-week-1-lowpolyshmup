﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeController : MonoBehaviour 
{
	public static float Volume = 1f;

	private Slider slider;

	private void Start() 
	{
		slider = GetComponent<Slider>();
	}

	private void Update() 
	{
		Volume = slider.value;
	}
}
