﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour 
{
	public GameObject gameController;

	public void OnPlayClicked()
	{
		GameController.Instance.mainMenu.menuOpenCloseController.showMenu = false;
		GameController.Instance.GameStart();
	}

	public void OnExitClicked()
	{
		GameController.Instance.ExitGame();
	}

	public void OnOptionsClicked()
	{
		GameController.Instance.GameReset();
	}

	public void OnCreditsClicked()
	{
		GameController.Instance.credits.menuOpenCloseController.showMenu = true;
		GameController.Instance.mainMenu.menuOpenCloseController.showMenu = false;
	}

	public void OnCreditsBackClicked()
	{
		GameController.Instance.credits.menuOpenCloseController.showMenu = false;
		GameController.Instance.mainMenu.menuOpenCloseController.showMenu = true;
	}
}
