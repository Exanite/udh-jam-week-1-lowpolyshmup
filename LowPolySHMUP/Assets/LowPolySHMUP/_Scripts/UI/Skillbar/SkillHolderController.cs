﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Exanite.Skills;

public class SkillHolderController : MonoBehaviour 
{
	public EntityStat statToTrack;
	public SkillData trackedSkillData;

	public SkillToTrack skillToTrack;

	[SerializeField]private GameObject _icon;
	[SerializeField]private GameObject _cooldown;

	private Image _cooldownImage;
	private Image _iconImage;

	private void Start() 
	{
		_icon = transform.Find("Slot/Icon").gameObject;
		_cooldown = transform.Find("Slot/Icon/Cooldown").gameObject;

		_cooldownImage = _cooldown.GetComponent<Image>();
		_iconImage = _icon.GetComponent<Image>();
	}

	private void Update() 
	{
		if(statToTrack != null)
		{
			TrackSkill();
		}
	}

	private void TrackSkill()
	{
		switch(skillToTrack)
		{
			case SkillToTrack.attack:
				trackedSkillData = statToTrack.skillDataOne;
				if(!trackedSkillData) {break;}
				UpdateCooldown(statToTrack.timeSinceLastAttack, trackedSkillData.cooldown / (trackedSkillData.useAttackSpeed ? statToTrack.attackSpeed.FinalValue : statToTrack.cooldownReduction.FinalValue));
				break;
			case SkillToTrack.skillOne:
				trackedSkillData = statToTrack.skillDataTwo;
				if(!trackedSkillData) {break;}
				UpdateCooldown(statToTrack.timeSinceLastSkillOne, trackedSkillData.cooldown / (trackedSkillData.useAttackSpeed ? statToTrack.attackSpeed.FinalValue : statToTrack.cooldownReduction.FinalValue));
				break;
			case SkillToTrack.skillTwo:
				trackedSkillData = statToTrack.skillDataThree;
				if(!trackedSkillData) {break;}
				UpdateCooldown(statToTrack.timeSinceLastSkillTwo, trackedSkillData.cooldown / (trackedSkillData.useAttackSpeed ? statToTrack.attackSpeed.FinalValue : statToTrack.cooldownReduction.FinalValue));
				break;
			case SkillToTrack.skillThree:
				trackedSkillData = statToTrack.skillDataFour;
				if(!trackedSkillData) {break;}
				UpdateCooldown(statToTrack.timeSinceLastSkillThree, trackedSkillData.cooldown / (trackedSkillData.useAttackSpeed ? statToTrack.attackSpeed.FinalValue : statToTrack.cooldownReduction.FinalValue));
				break;
			case SkillToTrack.skillFour:
				trackedSkillData = statToTrack.skillDataFive;
				if(!trackedSkillData) {break;}
				UpdateCooldown(statToTrack.timeSinceLastSkillFour, trackedSkillData.cooldown / (trackedSkillData.useAttackSpeed ? statToTrack.attackSpeed.FinalValue : statToTrack.cooldownReduction.FinalValue));
				break;
			default:
				break;
		}

		if(trackedSkillData)
		{
			_iconImage.sprite = trackedSkillData.skillIcon;
		}
	}

	private void UpdateCooldown(float timeSinceLastUse, float cooldown)
	{
		_cooldownImage.fillAmount = 1f - Mathf.Clamp(timeSinceLastUse / cooldown, 0f, 1f);
	}
}

namespace Exanite.Skills
{
	public enum SkillToTrack
	{
		attack,
		skillOne,
		skillTwo,
		skillThree,
		skillFour,
	}
}