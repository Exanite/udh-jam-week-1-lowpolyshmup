﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Exanite.Stats;

public class UIStatRegeneratingController : MonoBehaviour 
{
	public StatRegenerating trackedStat;

	private Image _bar;
	private Text _text;

	private void Start() 
	{
		_bar = transform.Find("Bar").gameObject.GetComponent<Image>();
		_text = transform.Find("Text").gameObject.GetComponent<Text>();;
	}

	private void Update() 
	{
		if(trackedStat != null)
		{
			_text.text = string.Format("{0}/{1}", Mathf.Round(trackedStat.Value), Mathf.Round(trackedStat.Max.FinalValue));
			_bar.fillAmount = Mathf.Clamp(trackedStat.Value/trackedStat.Max.FinalValue, 0f, 1f);
		}
		else
		{
			_text.text = "0/0";
			_bar.fillAmount = 1f;
		}
	}
}
