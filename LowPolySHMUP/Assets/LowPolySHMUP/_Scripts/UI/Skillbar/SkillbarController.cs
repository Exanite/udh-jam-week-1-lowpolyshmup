﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillbarController : MonoBehaviour 
{
	public GameObject entityToTrack;

	public GameObject skillHolderOne;
	public GameObject skillHolderTwo;
	public GameObject skillHolderThree;
	public GameObject skillHolderFour;
	public GameObject skillHolderFive;

	public GameObject healthBar;

	private EntityStat _statToTrack;

	private bool _update_firstRun = true;

	private void Update() 
	{
		if(!entityToTrack)
		{
			if((entityToTrack = GameObject.FindGameObjectWithTag("Player")) != null)
			{
				SetDependencies();
			}
		}
		else if(_update_firstRun)
		{
				SetDependencies();
		}
	}

	private void SetDependencies()
	{
		_statToTrack = entityToTrack.GetComponent<EntityStat>();

		skillHolderOne.GetComponent<SkillHolderController>().statToTrack = _statToTrack;
		skillHolderTwo.GetComponent<SkillHolderController>().statToTrack = _statToTrack;
		skillHolderThree.GetComponent<SkillHolderController>().statToTrack = _statToTrack;
		skillHolderFour.GetComponent<SkillHolderController>().statToTrack = _statToTrack;
		skillHolderFive.GetComponent<SkillHolderController>().statToTrack = _statToTrack;
		healthBar.GetComponent<UIStatRegeneratingController>().trackedStat = _statToTrack.health;

		_update_firstRun = false;
	}
}
