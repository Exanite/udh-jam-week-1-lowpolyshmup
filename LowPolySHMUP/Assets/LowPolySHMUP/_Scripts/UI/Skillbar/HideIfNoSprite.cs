﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideIfNoSprite : MonoBehaviour 
{
	private Image _image;
	private bool showingImage = true;

	private void Start()
	{
		_image = GetComponent<Image>();
	}

	private void Update() 
	{
		if(showingImage && _image.sprite == null)
		{
			_image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0f);
			showingImage = false;
		}
		else if(!showingImage && _image.sprite != null)
		{
			_image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 1f);
			showingImage = true;
		}
	}
}
