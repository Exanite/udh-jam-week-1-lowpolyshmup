﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;
using Exanite.Entity;

public class EntityStat : MonoBehaviour 
{
	public AudioClip death;

	public GameObject sparkPrefab;
	public SkillData skillDataOne;
	public SkillData skillDataTwo;
	public SkillData skillDataThree;
	public SkillData skillDataFour;
	public SkillData skillDataFive;

	public StatRegenerating health;
	public StatHolder damage;
	public StatHolder movementSpeed;
	public StatHolder attackSpeed;
	public StatHolder cooldownReduction;

	public Faction faction = Faction.Enemy;

	public float timeSinceLastAttack;
	public float timeSinceLastSkillOne;
	public float timeSinceLastSkillTwo;
	public float timeSinceLastSkillThree;
	public float timeSinceLastSkillFour;

	public bool isAlive = true;

	private void Start() 
	{
		health.Value = health.Max.FinalValue;
	}

	private void Update()
	{
		UpdateHealth();
		timeSinceLastAttack += Time.deltaTime;
		timeSinceLastSkillOne += Time.deltaTime;
		timeSinceLastSkillTwo += Time.deltaTime;
		timeSinceLastSkillThree += Time.deltaTime;
		timeSinceLastSkillFour += Time.deltaTime;
	}

	private void UpdateHealth()
	{
		if(health.Value <= 0)
		{
			OnDeath();
		}

		health.Regenerate();
	}

	public void TakeDamage(float amount)
	{
		health.Value -= amount;
	}

	private void OnDeath()
	{
		isAlive = false;

		GameObject spark = Instantiate(sparkPrefab, transform.position, transform.rotation);
		spark.GetComponent<SetColor>().color = gameObject.GetComponent<SetColor>().color;
		AudioSource.PlayClipAtPoint(death, transform.position + (Vector3.up * Camera.main.transform.position.y), 0.75f * VolumeController.Volume);

		Destroy(gameObject);
	}
}