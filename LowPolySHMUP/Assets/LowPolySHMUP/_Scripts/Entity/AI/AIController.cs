﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : EntityController 
{
	public GameObject target;
	public float searchRange = 15f;

	new public virtual void Start()
	{
		base.Start();
		InvokeRepeating("AIUpdate", Random.Range(0f, 0.2f), 0.25f);
	}

	public virtual void AIUpdate()
	{	
		target = (target == null || (Vector3.Distance(target.transform.position, transform.position)) > searchRange * 1.5f) ? GetTarget() : target;
	}

	new public virtual void Update()
	{
		base.Update();
	}

	public virtual GameObject GetTarget()
	{
		Collider[] colliders = Physics.OverlapSphere(transform.position, searchRange); //Get GameObjects in range

		GameObject closestEnemy = null;
		float closestEnemyRange = Mathf.Infinity;

		foreach (Collider collider in colliders)
		{
			EntityStat statOther;
			if((statOther = collider.gameObject.GetComponent<EntityStat>()) != null) //Get GameObjects with EntityStat in range
			{
				if(statOther.faction != m_entityStat.faction) //Get enemy faction(faction not equal to own)
				{
					if(Vector3.Distance(collider.gameObject.transform.position, transform.position) < closestEnemyRange) //Get closest enemy
					{
						closestEnemy = collider.gameObject;
						closestEnemyRange = Vector3.Distance(collider.gameObject.transform.position, transform.position);
					}
				}
			}
		}

		return closestEnemy;
	}
}

/* Look into this at somepoint?!?

return Physics.OverlapSphere(transform.position, searchRange)
            .Where(t => { EntityStat stat = t.GetComponent<EntityStat>(); return stat != null && stat.faction != _mEntityStat.faction })
            .Select(t => new { distance = Vector3.Distance(t.transform.position, transform.position), enemy = t })
            .OrderBy(t => t.distance)
            .FirstOrDefault()?.enemy;

 */