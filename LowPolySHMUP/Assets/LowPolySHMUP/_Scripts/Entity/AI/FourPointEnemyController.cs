﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;

public class FourPointEnemyController : AIController 
{
	public float closeRange = 3f;
	public bool canMove = true;
	public Vector3 moveDir = Vector3.zero;

	new public virtual void Start()
	{
		base.Start();

		_lastDir = Random.Range(0, 2) == 0;
	}

	new public virtual void Update()
	{
		base.Update();

		if(target)
		{
			AttackTarget();
		}
	}

	private void AttackTarget()
	{
		if(!canMove && !_isSpinning)
		{
			StartCoroutine(Spin(0.5f));
		}
		else if(canMove && !_isMoving)
		{
			Vector3 dest = GetDestination();

			StartCoroutine(MoveToDestination(dest));
		}

		if(m_entityStat.timeSinceLastAttack >= m_entityStat.skillDataOne.cooldown/m_entityStat.attackSpeed.FinalValue)
		{
			m_entityStat.timeSinceLastAttack = 0f;
			audioSource.PlayOneShot(shoot, 0.5f * VolumeController.Volume);
			m_skillSpawner.SpawnSkill(m_entityStat.skillDataOne);
		}
	}

	private bool _lastDir = false;

	private Vector3 GetDestination()
	{
		Vector3 dest = transform.position;

		if(_lastDir) //move left(-x) or right(+x)
		{
			dest = Vector3.Lerp(transform.position, new Vector3(target.transform.position.x, transform.position.y, transform.position.z), 0.5f);

			_lastDir = false;
		}
		else //move up(+z) or down(-z)
		{
			dest = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, target.transform.position.z), 0.5f);
			_lastDir = true;
		}

		return dest;
	}

	private bool _isSpinning = false;

	IEnumerator Spin(float duration)
	{
		StatModifier statModifierSpd = new StatModifier(4f, StatModifierType.Mult, this);
		m_entityStat.attackSpeed.AddModifier(statModifierSpd);
		
		_isSpinning = true;
		float startRotation = 0f;
		float endRotation = 360.0f;
		float timePassed = 0.0f;
		while(timePassed < duration)
		{
			timePassed += Time.deltaTime;
			float yRotation = Mathf.SmoothStep(startRotation, endRotation, timePassed / duration) % 360.0f;
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, yRotation, transform.eulerAngles.z);

			if(timePassed > duration)
			{
				canMove = true;
			}

			yield return null;
		}
		_isSpinning = false;
		m_entityStat.attackSpeed.RemoveModifier(statModifierSpd);
	}

	private bool _isMoving = false;

	IEnumerator MoveToDestination(Vector3 destination)
	{
		_isMoving = true;
		bool tooCloseToEntity = false;
		
		Vector3 lastPosition;
		lastPosition = transform.position;
		bool stuck = false;

		while(transform.position != destination && !tooCloseToEntity && !stuck)
		{
			Collider[] colliders = null;
			if((colliders = Physics.OverlapBox(transform.position, Vector3.one * ((transform.localScale.x + transform.localScale.z) / 2), Quaternion.identity, 1 << 10)) != null)
			{
				foreach(Collider collider in colliders)
				{
					if(collider.gameObject != gameObject)
					{
						tooCloseToEntity = true;
					}
				}
			}

			transform.position = Vector3.MoveTowards(transform.position, destination, m_entityStat.movementSpeed.FinalValue * Time.deltaTime);

			if(lastPosition == transform.position)
			{
				stuck = true;
			}
			lastPosition = transform.position;

			yield return null;
		}

		canMove = false;
		_isMoving = false;
	}
}
