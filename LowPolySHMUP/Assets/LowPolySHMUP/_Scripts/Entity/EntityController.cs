﻿using System.Collections;
using System.Collections.Generic;
using Exanite.Stats;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(EntityStat))]
[RequireComponent(typeof(SkillSpawner))]
[RequireComponent(typeof(AudioSource))]
public abstract class EntityController : MonoBehaviour 
{
	public bool smoothMovement = false;
	public float movementSmoothing = 10f;
	public Vector3 targetMovementDirectionSpeed = Vector3.zero;

	public AudioClip shoot;

	protected Rigidbody m_rigidBody;
	protected EntityStat m_entityStat;
	protected SkillSpawner m_skillSpawner;
	protected AudioSource audioSource;

	[SerializeField]private Vector3 _currentMovementDirectionSpeed = Vector3.zero;

	public virtual void Start()
	{
		m_rigidBody = GetComponent<Rigidbody>();
		m_entityStat = GetComponent<EntityStat>();
		m_skillSpawner = GetComponent<SkillSpawner>();
		audioSource = GetComponent<AudioSource>();

		m_rigidBody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
	}

	public virtual void Update()
	{
		ApplyMovementDirectionSpeed(targetMovementDirectionSpeed);
	}

	public virtual void ApplyMovementDirectionSpeed(Vector3 targetMovementDirectionSpeed)
	{
		_currentMovementDirectionSpeed = Vector3.Lerp(_currentMovementDirectionSpeed, targetMovementDirectionSpeed, smoothMovement ? Time.deltaTime * movementSmoothing : 1f);

		m_rigidBody.velocity = new Vector3
		(
			(_currentMovementDirectionSpeed.x * m_entityStat.movementSpeed.FinalValue), 
			m_rigidBody.velocity.y, 
			(_currentMovementDirectionSpeed.z * m_entityStat.movementSpeed.FinalValue)
		);
	}
}
