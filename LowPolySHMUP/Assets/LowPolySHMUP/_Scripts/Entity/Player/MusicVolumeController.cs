﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicVolumeController : MonoBehaviour 
{
	private AudioSource audioSource;

	private void Start() 
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Update() 
	{
		audioSource.volume = VolumeController.Volume * 0.3f;
	}
}
