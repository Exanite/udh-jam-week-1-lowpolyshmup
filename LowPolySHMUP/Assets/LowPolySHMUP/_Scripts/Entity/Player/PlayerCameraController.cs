﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PlayerCameraController : MonoBehaviour 
{
	public GameObject target;
	public Vector3 offset;

	public float damping = 10f;
	public float maxDistance = 10f;

	private Vector3 _currentVelocity;
	private PlayerController _mPlayerController;

	private void Start()
	{
		target = (target == null) ? GameObject.FindWithTag("Player") : target;
		_mPlayerController = (target != null) ? target.GetComponent<PlayerController>() : null;
	}

	private void FixedUpdate() 
	{
		if(target)
		{
			Vector3 mouseOffset = Vector3.Slerp(_mPlayerController.mouseOffsetFromPlayer, Vector3.zero, 0.75f);
			mouseOffset = Vector3.ClampMagnitude(mouseOffset, maxDistance);

			transform.position = Vector3.SmoothDamp(transform.position, target.transform.position + offset + mouseOffset, ref _currentVelocity, damping);
		}
		else
		{
			target = GameObject.FindWithTag("Player");
		}

		if(_mPlayerController == null && target)
		{
			_mPlayerController = target.GetComponent<PlayerController>();
		}
	}
}
