﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Stats;

public class PlayerController : EntityController 
{
	public Keybinds mKeybinds;

	#region Variables: Movement and Control
	public bool controllable = true;

	public Vector3 mouseWorldPosition;
	public Vector3 mouseOffsetFromPlayer;

	public float movementForwardMulti = 1.25f;
	public bool isSprinting = false;
	public float sprintMult = 1.5f;
	public float sprintCooldown = 1f;
	public StatRegenerating sprintTime;
	#endregion

	new private void Start()
	{
		base.Start();

		sprintTime = new StatRegenerating(5f, 2f);
		smoothMovement = true;
	}

	new private void Update()
	{
		base.Update();

		targetMovementDirectionSpeed = UpdateMovementDirectionSpeed();
		UpdateSprinting();
		UpdateMouseTarget();
		UpdateLookDirection();
		UpdateSkillInput();
	}

	private Vector3 UpdateMovementDirectionSpeed()
	{
		if(!controllable)
		{
			return Vector3.zero;
		}

		Vector3 movementDirectionSpeed = Vector3.zero;

		if(Input.GetKey(mKeybinds.MovementUp))
		{
			movementDirectionSpeed += Vector3.forward;
		}
		if(Input.GetKey(mKeybinds.MovementDown))
		{
			movementDirectionSpeed -= Vector3.forward;
		}
		if(Input.GetKey(mKeybinds.MovementRight))
		{
			movementDirectionSpeed += Vector3.right;
		}
		if(Input.GetKey(mKeybinds.MovementLeft))
		{
			movementDirectionSpeed -= Vector3.right;
		}

		float lookRotation = transform.rotation.eulerAngles.y;
		if(movementDirectionSpeed != Vector3.zero)
		{
			float moveRotation = Quaternion.LookRotation(movementDirectionSpeed, Vector3.up).eulerAngles.y;
			float rotationDifference = Mathf.DeltaAngle(lookRotation, moveRotation);
			float _forwardMovementMulti = Mathf.Lerp(movementForwardMulti, 1f, Mathf.Abs(rotationDifference/180));
			movementDirectionSpeed *= _forwardMovementMulti * (isSprinting ? sprintMult : 1f);
		}

		return movementDirectionSpeed;
	}

	private float m_sprintCooldown = 0f;
	private bool m_canSprint = true;

	private void UpdateSprinting()
	{
		if(m_canSprint && Input.GetKey(mKeybinds.MovementSprinting) && sprintTime.Value > 0)
		{
			isSprinting = true;
		}
		else
		{
			isSprinting = false;
		}

		if(!m_canSprint || m_sprintCooldown > 0f) //if sprint is disabled
		{
			m_sprintCooldown -= Time.deltaTime;

			if(m_sprintCooldown < 0f && sprintTime.Value > sprintTime.Max.FinalValue / 2f)
			{
				m_canSprint = true;
			}
		}

		if(isSprinting)
		{
			sprintTime.Value -= 1f * Time.deltaTime;

			if(sprintTime.Value <= 0)
			{
				m_sprintCooldown = sprintCooldown; //Disable sprinting for a bit
				m_canSprint = false;
			}
		}
		else if(m_sprintCooldown <= 0) //If sprint regen cooldown is less than 0 allow regeneration
		{
			sprintTime.Regenerate();
		}
	}

	private void UpdateMouseTarget()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if(Physics.Raycast(ray, out hit, 100f, 1 << 9))
		{
			mouseWorldPosition = hit.point;

			mouseOffsetFromPlayer = mouseWorldPosition - transform.position;
		}
	}

	private void UpdateLookDirection()
	{
		Vector3 lookPosition = new Vector3(mouseWorldPosition.x, transform.position.y, mouseWorldPosition.z);
		lookPosition -= transform.position;
		if(lookPosition != Vector3.zero)
		{
			transform.rotation = Quaternion.LookRotation(lookPosition, Vector3.up);
		}
	}

	private void UpdateSkillInput()
	{
		if(Input.GetKey(mKeybinds.LeftClick) && m_entityStat.timeSinceLastAttack >= m_entityStat.skillDataOne.cooldown/m_entityStat.attackSpeed.FinalValue)
		{
			m_skillSpawner.SpawnSkill(m_entityStat.skillDataOne);
			audioSource.PlayOneShot(shoot, 0.9f * VolumeController.Volume);
			m_entityStat.timeSinceLastAttack = 0f;
		}
	}
}