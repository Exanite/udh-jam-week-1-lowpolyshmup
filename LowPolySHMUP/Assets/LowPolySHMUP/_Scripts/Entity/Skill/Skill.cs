﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exanite.Entity;
using Exanite.Stats;

[RequireComponent(typeof(Rigidbody))]
public abstract class Skill : MonoBehaviour 
{
	public GameObject owner;
	public EntityStat owner_EntityStat;

	[SerializeField]protected StatHolder m_damage;
	[SerializeField]protected Faction m_faction = Faction.Enemy;
	[SerializeField]protected float m_lifeDuration = 10f;
	[SerializeField]protected bool m_targetAlly = false;
	[SerializeField]protected bool m_destroyOnNonEntity = true;

	private Rigidbody m_Rigidbody;
	private SetColor m_SetColor;

	protected virtual void Start()
	{
		if(!owner) {Destroy(gameObject);}

		m_Rigidbody = GetComponent<Rigidbody>();
		m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
		m_SetColor = GetComponent<SetColor>();
		m_SetColor.color = owner.GetComponent<SetColor>().color;

		owner_EntityStat = (owner_EntityStat) ? owner_EntityStat : owner.GetComponent<EntityStat>();

		SetInitialStats();
	}

	protected virtual void Update()
	{
		m_lifeDuration -= Time.deltaTime;
		if(m_lifeDuration <= 0)
		{
			OnSkillDestroy();
		}
	}

	protected virtual void SetInitialStats()
	{
		m_damage.statModifiers.AddRange(owner_EntityStat.damage.GetCombinedModifiers());
		m_faction = owner_EntityStat.faction;
	}

	protected virtual void OnTriggerEnter(Collider other)
	{
		if(!(other.gameObject.layer == 0 || other.gameObject.layer == 10)) {return;}
		if(other.CompareTag("Skill")) {return;}

		EntityStat statOther = null;
		if((statOther = other.gameObject.GetComponent<EntityStat>()) != null)
		{
			if(!m_targetAlly && statOther.faction != m_faction)
			{
				OnHit(other.gameObject);
			}
		}
		else if(m_destroyOnNonEntity)
		{
			OnSkillDestroy();
		}
	}

	protected abstract void OnHit(GameObject target);

	protected virtual void OnSkillDestroy()
	{
		Destroy(gameObject);
	}
}