﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillProjectile : Skill 
{
	[SerializeField]protected float m_speed = 10f;
	[SerializeField]protected float m_maxDistance = 25f;
	[SerializeField]protected int m_piercesLeft = 0;

	private float _distanceTraveled = 0f;

	private void FixedUpdate()
	{
		transform.position += transform.forward * m_speed * Time.deltaTime;
		_distanceTraveled += m_speed * Time.deltaTime;

		if(_distanceTraveled >= m_maxDistance)
		{
			OnSkillDestroy();
		}
	}

	protected override void OnHit(GameObject target)
	{
		EntityStat target_entityStat = target.GetComponent<EntityStat>();
		target_entityStat.TakeDamage(m_damage.FinalValue);
		if(GameController.Instance.debugMode) {print(string.Format("{0} took {1} damage", target.name, m_damage.FinalValue));}
		
		m_piercesLeft--;
		if(m_piercesLeft < 0)
		{
			OnSkillDestroy();
		}
	}
}
