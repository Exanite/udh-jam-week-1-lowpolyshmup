﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillSpawner : MonoBehaviour 
{
	public List<Transform> skillSpawns;

	private void Start()
	{
		foreach(Transform child in transform)
		{
			if(child.gameObject.name.Contains("SkillSpawn"))
			{
				skillSpawns.Add(child);
			}
		}
	}
	
	public void SpawnSkill(SkillData skillData)
	{
		foreach(Transform transform in skillSpawns)
		{
			GameObject spawnedSkill = Instantiate(skillData.skillPrefab, transform.position + (transform.forward * 0.5f), transform.rotation);

			Skill skill = spawnedSkill.GetComponent<Skill>();
			skill.owner = gameObject;
		}
	}
}
