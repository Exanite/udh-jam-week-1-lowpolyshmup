﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillProjectile_SparkOnDeath : SkillProjectile 
{
	public GameObject sparkPrefab;

	protected override void OnSkillDestroy() 
	{
		GameObject spark = Instantiate(sparkPrefab, transform.position - (transform.forward * 0.2f), transform.rotation);
		spark.GetComponent<SetColor>().color = gameObject.GetComponent<SetColor>().color;

		base.OnSkillDestroy();
	}
}
