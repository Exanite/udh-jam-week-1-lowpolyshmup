﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Exanite;
using Exanite.Stats;
using Exanite.UI;
using Exanite.Game;

public class GameController : MonoBehaviour 
{
	public static GameController Instance;
	public bool debugMode = false;

	public Keybinds keybinds;
	public GameData gameData;

	public GameObject canvas;
	public MenuWindow skillbar;
	public MenuWindow mainMenu;
	public MenuWindow credits;
	public AnnouncerController announcer;

	public GameObject player;
	public bool playerSpawned = false;

	public List<GameObject> enemies;

	public bool gameStarted = false;
	public int waveNumber = 0;
	[Range(0, 2)]public WaveState waveState = 0; //0 = Ended, 1 = Not started, 2 = Started

	private void Awake() 
	{
		if(!Instance)
		{
			Instance = this;
		}
		else
		{
			Debug.LogWarning("There is already a GameController instance in the scene");
		}

		canvas = (canvas) ? canvas : GameObject.Find("Canvas");
		skillbar = new MenuWindow(canvas.transform.Find("Skillbar").gameObject);
		mainMenu = new MenuWindow(canvas.transform.Find("MainMenu").gameObject);
		credits = new MenuWindow(canvas.transform.Find("Credits").gameObject);
		announcer = (announcer) ? announcer : canvas.transform.Find("Announcer").gameObject.GetComponent<AnnouncerController>();
	}

	private void Start() 
	{
		mainMenu.menuOpenCloseController.showMenu = true;
	}

	private void Update() 
	{
		CheckWaves();

		if(playerSpawned && !player) //If player dead
		{
			playerSpawned = false;
			mainMenu.menuOpenCloseController.showMenu = true;
			StartCoroutine(announcer.ShowAnnouncement(string.Format("You have died on Wave {0}, press the Reset Button in the Main Menu to play again", waveNumber), 1f, 20f, false));
		}

		if(gameStarted && Input.GetKeyDown(keybinds.Menu))
		{
			mainMenu.menuOpenCloseController.showMenu = true;
		}

		if(mainMenu.menuOpenCloseController.showMenu)
		{
			Time.timeScale = 0.1f;
			Time.fixedDeltaTime = Time.timeScale * 0.02f;
		}
		else
		{
			Time.timeScale = 1f;
			Time.fixedDeltaTime = Time.timeScale * 0.02f;
		}
	}

	#region Game reset/exit
	public void GameReset()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void ExitGame()
	{
		Application.Quit();
	}
	#endregion

	#region Game wave system:
	public void GameStart()
	{
		if(gameStarted)
		{
			return;
		}

		if(GameController.Instance.debugMode) {Debug.Log("The game has started");}
		gameStarted = true;

		player = Instantiate(gameData.player, Vector3.zero, Quaternion.identity);
		playerSpawned = true;
		skillbar.menuOpenCloseController.showMenu = true;

		waveState = 0;
	}

	private int _lastEnemiesRemaining;

	public int EnemiesRemaining
	{
		get
		{
			int enemiesRemaining = enemies.Count - enemies.RemoveAll(enemy => enemy == null);;
			if(GameController.Instance.debugMode && _lastEnemiesRemaining != enemiesRemaining) {Debug.Log(string.Format("There are {0} enemies remaining", enemiesRemaining));}
			_lastEnemiesRemaining = enemiesRemaining;
			return enemiesRemaining;
		}
	}

	private bool _noMoreWaves = false;

	private void CheckWaves()
	{
		if(!gameStarted)
		{
			return;
		}

		if(waveState == WaveState.ReadyToStart && waveNumber <= gameData.waves.Count)
		{
			StartCoroutine(SpawnEnemies(waveNumber));
			waveState = WaveState.Ended;
		}
		else if(waveState == 0 && EnemiesRemaining == 0) //Prepares for next wave
		{
			waveNumber++;
			waveState = WaveState.ReadyToStart;
		}
		else if (waveState == WaveState.ReadyToStart && waveNumber >= gameData.waves.Count && !_noMoreWaves)
		{
			_noMoreWaves = true;
			StartCoroutine(announcer.ShowAnnouncement(string.Format("Wave {0} does not exist yet... You reached the end, Congrats! Press ESCAPE to restart!", waveNumber), 1f, 20f, false));
			if(GameController.Instance.debugMode) {print("There are no more waves");}
		}
	}

	IEnumerator SpawnEnemies(int waveNumber)
	{
		waveState = WaveState.Ended;
		StartCoroutine(announcer.ShowAnnouncement(string.Format("Wave {0}", waveNumber), 1f, 2f));
		if(GameController.Instance.debugMode) {Debug.Log(string.Format("Wave {0} has started...", waveNumber));}
		waveNumber--;

		foreach(Spawns spawn in gameData.waves[waveNumber].spawns)
		{
			foreach(GameObject enemy in spawn.enemiesToSpawn)
			{
				GameObject spawnedEnemy = Instantiate(enemy, spawn.spawnpoint, Quaternion.identity);
				enemies.Add(spawnedEnemy);

				EntityStat enemyStat = spawnedEnemy.GetComponent<EntityStat>();
				enemyStat.health.Max.AddModifier(new StatModifier(gameData.waves[waveNumber].enemyHealthMultiplier, StatModifierType.Mult, this));
				enemyStat.damage.AddModifier(new StatModifier(gameData.waves[waveNumber].enemyDamageMultiplier, StatModifierType.Mult, this));
				
				yield return new WaitForSeconds(gameData.waves[waveNumber].timeBetweenEachSpawn);
			}
		}
		waveState = 0;
	}
	#endregion
}