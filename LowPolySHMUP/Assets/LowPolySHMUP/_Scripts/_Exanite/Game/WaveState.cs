﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.Game
{
	public enum WaveState
	{
		Ended = 0,
		ReadyToStart = 1,
		Started = 2,
	}
}