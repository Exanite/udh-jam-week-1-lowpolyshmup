﻿using UnityEngine;

namespace Exanite
{
	public static class VectorThree
	{
		public static Vector3 SmoothStep(Vector3 from, Vector3 to, float t)
		{
			Vector3 temp;

			temp = new Vector3(Mathf.SmoothStep(from.x, to.x, t), Mathf.SmoothStep(from.y, to.y, t), Mathf.SmoothStep(from.z, to.z, t));

			return temp;
		}
	}
}