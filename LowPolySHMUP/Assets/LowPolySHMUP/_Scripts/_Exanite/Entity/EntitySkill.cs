﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.Entity
{
	[System.Serializable]
	public class EntitySkill
	{
		public SkillData SkillData;
		public float lastAttackTime = 0f;

		public EntitySkill(SkillData skillData)
		{
			SkillData = skillData;
			lastAttackTime = 0f;
		}

		public bool CanUseSkill(float attackSpeed)
		{
			if(Time.time - lastAttackTime > SkillData.cooldown / ((SkillData.useAttackSpeed) ? attackSpeed : 1f))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public void UseSkill(SkillSpawner skillSpawner, SkillData skillData)
		{
			lastAttackTime = Time.time;


		}
	}
}