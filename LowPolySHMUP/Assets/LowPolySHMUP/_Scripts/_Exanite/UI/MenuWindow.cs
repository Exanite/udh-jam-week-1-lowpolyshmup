﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Exanite.UI
{
	[System.Serializable]
	public class MenuWindow
	{
		public GameObject menu;
		public MenuOpenCloseController menuOpenCloseController;

		public MenuWindow(GameObject menu)
		{
			if((menuOpenCloseController = menu.GetComponent<MenuOpenCloseController>()) == null)
			{
				menuOpenCloseController = menu.AddComponent<MenuOpenCloseController>();
			}

			menu.SetActive(true);
		}
	}
}