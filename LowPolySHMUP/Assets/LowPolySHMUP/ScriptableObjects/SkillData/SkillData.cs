﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkillData", menuName = "LowPolySHMUP/SkillData", order = 0)]
public class SkillData : ScriptableObject 
{
	public GameObject skillPrefab;
	public Sprite skillIcon;

	public bool useAttackSpeed;
	public float cooldown;
}