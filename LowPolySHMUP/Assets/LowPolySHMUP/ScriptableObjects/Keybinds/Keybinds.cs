﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Keybinds", menuName = "LowPolySHMUP/Keybinds", order = 0)]
public class Keybinds : ScriptableObject 
{
	public KeyCode Menu = KeyCode.Escape;
	public KeyCode LeftClick = KeyCode.Mouse0;
	public KeyCode RightClick = KeyCode.Mouse1;

	public KeyCode MovementUp = KeyCode.W;
	public KeyCode MovementDown = KeyCode.S;
	public KeyCode MovementRight = KeyCode.D;
	public KeyCode MovementLeft = KeyCode.A;
	public KeyCode MovementSprinting = KeyCode.LeftShift;
}