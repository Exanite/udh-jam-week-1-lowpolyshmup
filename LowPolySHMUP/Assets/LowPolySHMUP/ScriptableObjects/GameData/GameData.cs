﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "LowPolySHMUP/GameData", order = 0)]
public class GameData : ScriptableObject
{
	public GameObject player;

	public List<Waves> waves;
}

[System.Serializable]
public class Waves
{
	public float enemyDamageMultiplier = 1f;
	public float enemyHealthMultiplier = 1f;
	public float timeBetweenEachSpawn = 5f;
	public List<Spawns> spawns;
}

[System.Serializable]
public class Spawns
{
	public Vector3 spawnpoint;
	public List<GameObject> enemiesToSpawn;
}